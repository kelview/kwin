# Lithuanian translations for kwin package.
# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the kwin package.
# Automatically generated, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kwin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-18 01:00+0000\n"
"PO-Revision-Date: 2023-02-18 01:00+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: kcmvirtualkeyboard.cpp:61
#, kde-format
msgid "None"
msgstr ""

#: kcmvirtualkeyboard.cpp:65
#, kde-format
msgid "Do not use any virtual keyboard"
msgstr ""

#: package/contents/ui/main.qml:16
#, kde-format
msgid "This module lets you choose the virtual keyboard to use."
msgstr ""
